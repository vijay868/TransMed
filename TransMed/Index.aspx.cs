﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using Ionic.Zip;


namespace TransMed
{
    public partial class Index : PageBase
    {                
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!isUserAuth)
                Response.Redirect("login.aspx");

            if (!IsPostBack)
            {
                hdnDate.Value = DateTime.Now.ToString("MM/dd/yyyy");                
                BindDataToGrid(GetAllFiles());
                hdnTypeOfSearch.Value = "0";
                BindDoctors();
                Label lblUserName = (Label)Page.Master.FindControl("lblUserName");
                lblUserName.Text = userName;
            }

            pnlDoctor.Visible = userRole.ToLower() == "Admin".ToLower();
            ppnlDoctor.Visible = userRole.ToLower() == "admin";
            hdnUrl.Value = UrlSuffix;

            
        }

        protected void BindDataToGrid(DataTable dt)
        {
            grdFiles.Visible = btnDownload.Visible = lblRecords.Visible = dt.Rows.Count > 0;
            lblGrdStatus.Visible = !(dt.Rows.Count > 0);
            lblRecords.Text = "total records found: " + dt.Rows.Count.ToString();
            if (dt.Rows.Count > 0)
            {
                grdFiles.DataSource = dt;
                grdFiles.DataBind();
            }
        }

        protected void BindDoctors()
        {
            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ProcGetDoctors";                

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                ddlDoctor.DataSource = dt;
                ddlDoctor.DataTextField = "DoctorName";
                ddlDoctor.DataValueField = "DoctorCode";

                ddlDoctor.DataBind();

                doctor.DataSource = dt;
                doctor.DataTextField = "DoctorName";
                doctor.DataValueField = "DoctorCode";

                doctor.DataBind();
            }
        }

        protected DataTable GetAllFilesByDate()
        {
            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetAllFilesByDate2";

                cmd.Parameters.Add("@sortField", SqlDbType.VarChar);
                cmd.Parameters.Add("@sortDirection", SqlDbType.VarChar);
                cmd.Parameters.Add("@date", SqlDbType.VarChar);
                cmd.Parameters.Add("@GroupId", SqlDbType.VarChar);
                cmd.Parameters.Add("@DoctorId", SqlDbType.VarChar);

                cmd.Parameters["@sortField"].Value = SortField;
                cmd.Parameters["@sortDirection"].Value = SortDirection;
                cmd.Parameters["@date"].Value = hdnDate.Value;
                cmd.Parameters["@GroupId"].Value = userGroupCode;
                cmd.Parameters["@DoctorId"].Value = GetDoctorCode();

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                return dt;
            }
        }


        protected DataTable GetAllFiles()
        {

            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetAllFiles2";

                cmd.Parameters.Add("@sortField", SqlDbType.VarChar);
                cmd.Parameters.Add("@sortDirection", SqlDbType.VarChar);
                cmd.Parameters.Add("@GroupId", SqlDbType.VarChar);
                cmd.Parameters.Add("@DoctorId", SqlDbType.VarChar);

                cmd.Parameters["@sortField"].Value = SortField;
                cmd.Parameters["@sortDirection"].Value = SortDirection;
                cmd.Parameters["@GroupId"].Value = userGroupCode;
                cmd.Parameters["@DoctorId"].Value = GetDoctorCode();

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                return dt;
            }
        }

        protected string GetDoctorCode()
        {
            var code = "";
            if (userRole.ToLower() == "Admin".ToLower())
            {
                if (ddlDoctor.SelectedValue == "0" || hdnTypeOfSearch.Value == "2" || hdnTypeOfSearch.Value == "3")
                    code = "";
                else
                    code = ddlDoctor.SelectedValue;
            }
            else if(userRole.ToLower() == "Doctor".ToLower())
            {
                code = userDoctorCode;
            }
            return code;
        }


        protected void grdFiles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdFiles.PageIndex = e.NewPageIndex;
            BindDataToGrid(TypeOfSearchedData(txtSearch.Text.Trim()));            
        }

        protected void grdFiles_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (e.SortExpression.Trim() == this.SortField)
                this.SortDirection = (this.SortDirection == "DESC" ? "ASC" : "DESC");
            else
                this.SortDirection = "ASC";

            this.SortField = e.SortExpression;

            BindDataToGrid(TypeOfSearchedData(txtSearch.Text.Trim()));           
        }

        protected DataTable TypeOfSearchedData(string str)
        {
            switch (hdnTypeOfSearch.Value)
            {
                case "0":
                    return GetAllFiles();
                case "1":
                    return GetAllFilesByDate();
                case "2":
                    return SimpleSearchData(str);
                case "3":
                    return AdvancedSearchData();
                default:
                    return new DataTable();
            }
        }

        #region
        /*
         1 Stands for datepicker search
         2 stands for simple search
         3 stands for advanced search
        */
        #endregion

        string SortField
        {
            get
            {
                object o = ViewState["SortField"];
                if (o == null)
                    return "DateCreated";
                else
                    return (string)o;
            }
            set
            {
                ViewState["SortField"] = value;
            }
        }

        string SortDirection
        {
            get
            {
                object o = ViewState["SortDirection"];
                if (o == null)
                    return "DESC";
                else
                    return (string)o;
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }        

        protected DataTable SimpleSearchData(string search)
        {
            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SimpleSearch2";

                cmd.Parameters.Add("@search", SqlDbType.VarChar);
                cmd.Parameters.Add("@sortField", SqlDbType.VarChar);
                cmd.Parameters.Add("@sortDirection", SqlDbType.VarChar);
                cmd.Parameters.Add("@GroupId", SqlDbType.VarChar);
                cmd.Parameters.Add("@DoctorId", SqlDbType.VarChar);

                cmd.Parameters["@search"].Value = search;
                cmd.Parameters["@sortField"].Value = SortField;
                cmd.Parameters["@sortDirection"].Value = SortDirection;
                cmd.Parameters["@GroupId"].Value = userGroupCode;
                cmd.Parameters["@DoctorId"].Value = GetDoctorCode();

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                return dt;
            }
        }        

        protected void lnkDate_Click(object sender, EventArgs e)
        {
            hdnTypeOfSearch.Value = "1";
            BindDataToGrid(GetAllFilesByDate());            
        }

        protected void lnkSearch_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
            {
                hdnTypeOfSearch.Value = "2";
                BindDataToGrid(SimpleSearchData(txtSearch.Text.Trim()));
            }
        }

        protected void lnkAdSearch_Click(object sender, EventArgs e)
        {
            hdnTypeOfSearch.Value = "3";
            BindDataToGrid(AdvancedSearchData());            
        }

        protected DataTable AdvancedSearchData()
        {
            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                var cmd = new SqlCommand();

                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ProcAdvanceSearch2";

                //cmd.Parameters.Add("@date", SqlDbType.VarChar).Value = hdnDate.Value;
                cmd.Parameters.Add("@PatientFirstName", SqlDbType.VarChar).Value = txtpFirstName.Text.Trim();
                cmd.Parameters.Add("@PatientLastName", SqlDbType.VarChar).Value = txtpLastName.Text.Trim();
                //cmd.Parameters.Add("@DoctorName", SqlDbType.VarChar).Value = txtDoctorName.Text.Trim();
                //cmd.Parameters.Add("@GroupName", SqlDbType.VarChar).Value = txtGroupName.Text.Trim();
                cmd.Parameters.Add("@sortField", SqlDbType.VarChar);
                cmd.Parameters.Add("@sortDirection", SqlDbType.VarChar);

                cmd.Parameters["@sortField"].Value = SortField;
                cmd.Parameters["@sortDirection"].Value = SortDirection;                

                if (txtFromDt.Text.Trim() == "")
                    cmd.Parameters.Add("@fromDt", SqlDbType.VarChar).Value = DBNull.Value;
                else
                    cmd.Parameters.Add("@fromDt", SqlDbType.VarChar).Value = txtFromDt.Text.Trim();

                if (txtToDt.Text.Trim() == "")
                    cmd.Parameters.Add("@toDt", SqlDbType.VarChar).Value = DBNull.Value;
                else
                    cmd.Parameters.Add("@toDt", SqlDbType.VarChar).Value = txtToDt.Text.Trim();

                if (txtMrn.Text.Trim() == "")
                    cmd.Parameters.Add("@Mrn", SqlDbType.VarChar).Value = DBNull.Value;
                else
                    cmd.Parameters.Add("@Mrn", SqlDbType.VarChar).Value = txtMrn.Text.Trim();

                if (txtSsn.Text.Trim() == "")
                    cmd.Parameters.Add("@Ssn", SqlDbType.VarChar).Value = DBNull.Value;
                else
                    cmd.Parameters.Add("@Ssn", SqlDbType.VarChar).Value = txtSsn.Text.Trim();

                cmd.Parameters.Add("@doctorCode", SqlDbType.VarChar).Value = (userRole.ToLower() == "admin") ? (doctor.SelectedValue == "0" ? "" : doctor.SelectedValue) : userDoctorCode;

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                return dt;
            }
        }

        protected void ddlDoctor_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnTypeOfSearch.Value = "1";
            BindDataToGrid(GetAllFilesByDate());
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            
            List<string> lstRowIds = this.hdnchkval.Value.Split(new char[]
			{
				','
			}).ToList<string>();


            DataTable source = GetFileDetails(hdnchkval.Value);
            FileNotFoundException ex = new FileNotFoundException();
            List<string> tempList = new List<string>();
            try
            {
                using (ZipFile zipFile = new ZipFile())
                {
                    int i;
                    for (i = 0; i <= lstRowIds.Count - 1; i++)
                    {                        

                        DataRow dataRow = (from row in source.AsEnumerable()
                                      where row.Field<int>("FileId") == int.Parse(lstRowIds[i])
                                      select row).FirstOrDefault<DataRow>();

                        string fileName = base.Server.MapPath(string.Format("~/Reports/{0}/{1}", new object[]
						{							
							string.Format("{0}", Convert.ToDateTime(dataRow["DateCreated"]).ToString("MMddyyyy")),
							dataRow["FileName"]
						}));

                        FileInfo fileInfo = new FileInfo(fileName);
                        if (!fileInfo.Exists)
                        {
                            throw ex;
                        }
                        if (!tempList.Contains(Path.GetFileName(fileName)))
                        {
                            zipFile.AddFile(fileName, "files");
                            tempList.Add(Path.GetFileName(fileName));
                        }
                    }
                    base.Response.Clear();
                    base.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.zip",DateTime.Now.ToString("MM_dd_yyyy_hhmm")));
                    base.Response.ContentType = "application/zip";
                    zipFile.Save(base.Response.OutputStream);
                    base.Response.End();
                }
            }
            catch (FileNotFoundException)
            {
                base.Response.Redirect("error_404.html");
            }
        }

        protected DataTable GetFileDetails(string fileIds)
        {
            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetFileNameByFileId";

                cmd.Parameters.Add("@fileIds", SqlDbType.VarChar).Value = fileIds;

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                return dt;
            }
        }

        protected void grdFiles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = userRole.ToLower() == "admin";

                Literal ltrlViewPdf = e.Row.FindControl("ltrlBtnViewPdf") as Literal;

                ltrlViewPdf.Text = string.Format("<button id='btnViewPdf_{0}' onclick=\"viewPdf('{0}','{1}','{2}')\" type='button' class='btn btn-sm {3}'><i class='fa fa-eye'></i>&nbsp;<span>View </span></button>", 
                    DataBinder.Eval(e.Row.DataItem, "FileId"),
                    DataBinder.Eval(e.Row.DataItem, "FileName"),
                    DataBinder.Eval(e.Row.DataItem, "DateCreated"),
                    DataBinder.Eval(e.Row.DataItem, "IsViewed").ToString() == "True" ? "btn-info" : "btn-warning");

                Literal ltrlDownload = e.Row.FindControl("ltrlDownload") as Literal;

                ltrlDownload.Text = string.Format("<a class='btn {2} btn-sm' target='_blank' id='download_{3}' onclick=\"download('{0}','{1}','download_{3}')\" href='download.ashx?date={0}&fileName={1}&fileId={3}'><i class='fa fa-file-pdf-o'></i>&nbsp;<span>Download</span></a>",
                    DateTime.Parse(DataBinder.Eval(e.Row.DataItem, "DateCreated").ToString()).ToString("MMddyyyy"),
                    DataBinder.Eval(e.Row.DataItem, "FileName"),
                    DataBinder.Eval(e.Row.DataItem, "IsDownloaded").ToString() == "True" ? "btn-primary" : "btn-success",
                    DataBinder.Eval(e.Row.DataItem, "FileId"));
                

            }
            else if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = userRole.ToLower() == "admin";
            }
        }

        [System.Web.Services.WebMethod]
        public static void Viewed(string fileId)
        {
            isViewedOrDownloaded(fileId, "ProcUpdateIsViewed");
        }
    }
}
