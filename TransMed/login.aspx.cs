﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.SqlClient;
using System.Configuration;
using System.Data;


namespace TransMed
{
    public partial class login : PageBase
    {
        protected static string dbString = ConfigurationManager.ConnectionStrings["dbConn"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLoin_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtEmail.Text.Trim()) && !string.IsNullOrEmpty(txtPassword.Text.Trim()))
            {
                using (var conn = new SqlConnection(dbString))
                {
                    conn.Open();

                    var cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "ProcUserAuth";

                    cmd.Parameters.Add("@email", SqlDbType.VarChar);
                    cmd.Parameters.Add("@password", SqlDbType.VarChar);

                    cmd.Parameters["@email"].Value = txtEmail.Text.Trim();
                    cmd.Parameters["@password"].Value = txtPassword.Text.Trim();

                    var dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    var dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        var userObj = new userDetails()
                        {
                            name = dr["UserName"].ToString(),
                            email = dr["Email"].ToString(),
                            groupCode = dr["GroupId"].ToString(),
                            role = dr["Role"].ToString(),
                            doctorCode = dr["DoctorCode"].ToString(),
                            lastName = dr["LastName"].ToString(),
                            firstName = dr["FirstName"].ToString(),
                            userId = int.Parse(dr["RecordId"].ToString())
                        };
                        isUserAuth = true;
                        Session["SsnUserDetails"] = userObj;


                        Response.Redirect("index.aspx");
                    }
                    else
                    {
                        lblStatus.Text = "Invalid email or password";
                        lblStatus.Visible = true;
                    }                                     
                }
            }
        }       
    }

    public class userDetails
    {
        public string name { get; set; }
        public string email { get; set; }
        public string groupCode { get; set; }
        public string role { get; set; }
        public string doctorCode { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public int userId { get; set; }
    }
}