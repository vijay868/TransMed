﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace TransMed
{
    public class PageBase : Page
    {
        public string dbString
        {
            get 
            {
                return ConfigurationManager.ConnectionStrings["dbConn"].ToString();
            }
        }

        public static string dbConString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["dbConn"].ToString();
            }
        }

        public bool isUserAuth
        {
            get 
            {
                if (HttpContext.Current.Session["SsnUserAuth"] != null)
                    return true;
                else
                    return false;
            }
            set 
            {
                HttpContext.Current.Session["SsnUserAuth"] = value;
            }
        }

        public int userId
        {
            get
            {
                var obj = (userDetails)HttpContext.Current.Session["SsnUserDetails"];
                return obj.userId;
            }
        }

        public string userGroupCode
        {
            get 
            {
                var obj = (userDetails)HttpContext.Current.Session["SsnUserDetails"];
                return obj.groupCode;
            }
        }

        public string userDoctorCode
        {
            get
            {
                var obj = (userDetails)HttpContext.Current.Session["SsnUserDetails"];
                return obj.doctorCode == "0" ? "" : obj.doctorCode;
            }
        }

        public string userRole
        {
            get
            {
                var obj = (userDetails)HttpContext.Current.Session["SsnUserDetails"];
                return obj.role;
            }
        }

        public string userEmail
        {
            get
            {
                var obj = (userDetails)HttpContext.Current.Session["SsnUserDetails"];
                return obj.email;
            }
        }

        public string userName
        {
            get
            {
                var obj = (userDetails)HttpContext.Current.Session["SsnUserDetails"];
                return obj.lastName + ", " + obj.firstName;
            }
        }

        public string UrlBase
        {
            get { return UrlSuffix; }
        }

        protected string UrlSuffix
        {
            get
            {
                string strUrlSuffix;
                strUrlSuffix = System.Web.HttpContext.Current.Request.Url.Host;
                strUrlSuffix = strUrlSuffix + System.Web.HttpContext.Current.Request.ApplicationPath;
                if (strUrlSuffix.LastIndexOf("/") == strUrlSuffix.Length - 1)
                {
                    strUrlSuffix = strUrlSuffix.Substring(0, strUrlSuffix.Length - 1);
                }
                return strUrlSuffix;
            }
        }

        public static void isViewedOrDownloaded(string fileId, string spName)
        {
            using (var conn = new SqlConnection(dbConString))
            {
                conn.Open();

                var cmd = new SqlCommand();

                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = spName;

                cmd.Parameters.Add("@fileId", SqlDbType.VarChar).Value = fileId;

                cmd.ExecuteScalar();
            }
        }
    }    
}