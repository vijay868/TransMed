﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TransMed
{
    public partial class Layout : MasterPage
    {
        PageBase pageBase;
        protected void Page_Load(object sender, EventArgs e)
        {
            ltrlScript.Text = "";
            pageBase = new PageBase();
        }

        protected void lnkUpdate_Click(object sender, EventArgs e)
        {
            GetProfileDetails();

            var script = "<script type='text/javascript'>";
            script += "$('#userProfile').modal('show');";
            script += "</script>";

            ltrlScript.Text = script;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            
            using (var conn = new SqlConnection(pageBase.dbString))
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "UpdateUserDetails";

                cmd.Parameters.Add("@salutation", SqlDbType.VarChar).Value = txtSalutation.Text.Trim();
                cmd.Parameters.Add("@degree", SqlDbType.VarChar).Value = txtDegree.Text.Trim();
                cmd.Parameters.Add("@firstName", SqlDbType.VarChar).Value = txtFirstName.Text.Trim();
                cmd.Parameters.Add("@lastName", SqlDbType.VarChar).Value = txtLastName.Text.Trim();
                cmd.Parameters.Add("@middleName", SqlDbType.VarChar).Value = txtMiddleName.Text.Trim();
                cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = txtPhone.Text.Trim();
                cmd.Parameters.Add("@address", SqlDbType.VarChar).Value = txtAddress.Text.Trim();
                cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = txtCity.Text.Trim();
                cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = txtState.Text.Trim();
                cmd.Parameters.Add("@zipcode", SqlDbType.VarChar).Value = txtZipcode.Text.Trim();
                cmd.Parameters.Add("@fax", SqlDbType.VarChar).Value = txtFax.Text.Trim();
                cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = txtPassword.Text.Trim();
                cmd.Parameters.Add("@userId", SqlDbType.Int).Value = pageBase.userId;

                cmd.ExecuteScalar();

                Response.Redirect("Index.aspx");

            }
        }       

        protected void GetProfileDetails()
        {
            using (var conn = new SqlConnection(pageBase.dbString))
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ProcProfileDetails";

                cmd.Parameters.Add("@userId", SqlDbType.Int).Value = pageBase.userId;

                var dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    txtSalutation.Text = dr["Salutation"].ToString();
                    txtPassword.Text = dr["Password"].ToString();
                    txtFirstName.Text = dr["FirstName"].ToString();
                    txtLastName.Text = dr["LastName"].ToString();
                    txtMiddleName.Text = dr["MiddleName"].ToString();
                    txtDegree.Text = dr["Degree"].ToString();
                    txtAddress.Text = dr["Address"].ToString();
                    txtCity.Text = dr["City"].ToString();
                    txtState.Text = dr["State"].ToString();
                    txtZipcode.Text = dr["ZipCode"].ToString();
                    txtPhone.Text = dr["Phone"].ToString();
                    txtFax.Text = dr["Fax"].ToString();
                }
            }
        }
    }
}
