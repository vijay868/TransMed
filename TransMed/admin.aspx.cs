﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using Newtonsoft.Json;

namespace TransMed
{
    public partial class admin : System.Web.UI.Page
    {
        protected static string dbString = ConfigurationManager.ConnectionStrings["dbConn"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDoctors();
            }

            lblStatus.Text = "";
        }

        protected void BindDoctors()
        {
            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ProcGetDoctors";

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                ddlDoctor.DataSource = dt;

                ddlDoctor.DataTextField = "DoctorName";
                ddlDoctor.DataValueField = "DoctorCode";

                ddlDoctor.DataBind();
            }
        }

        [WebMethod]
        public static string PatientInfo(Search Obj)
        {
            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ProcGetPatientDetails";

                cmd.Parameters.Add("@doctorId", SqlDbType.VarChar).Value = Obj.doctor;
                cmd.Parameters.Add("@fname", SqlDbType.VarChar).Value = Obj.text.Split(' ')[0];
                cmd.Parameters.Add("@lname", SqlDbType.VarChar).Value = Obj.text.Split(' ')[1];

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                return JsonConvert.SerializeObject(dt);
            }
        }

        [WebMethod]
        public static string GetPatientsByDoctorId(Search Obj)
        {
            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ProcGetPatientsByDoctorId";

                cmd.Parameters.Add("@doctorId", SqlDbType.VarChar).Value = Obj.doctor;
                cmd.Parameters.Add("@search", SqlDbType.VarChar).Value = Obj.text;

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                return JsonConvert.SerializeObject(dt);
            }               
            
        }

        /*
        protected void ddlDoctor_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtPatientName.Text = string.Empty;
            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                var cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ProcGetPatientsByDoctorId";

                cmd.Parameters.Add("@doctorId", SqlDbType.VarChar).Value = ((DropDownList)sender).SelectedValue;

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                string Array = "";
                if (dt.Rows.Count > 0)
                {
                    Array = Array + "var ComArray = new Array(";
                    int i = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        i++;
                        Array += string.Format("\"{0}\"{1}", dr[0].ToString(), i < dt.Rows.Count ? "," : ");");
                    }
                }
                else
                    Array = "var ComArray = new Array();";

                string Script = "<script type=\"text/javascript\" language=\"javascript\">";
                Script += Array;
                Script += "</script>";

                ltrlArray.Text = Script;
            }
        }
        */
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var flag = false;
            try
            {
                if (fileUpload.HasFile && ddlDoctor.SelectedValue != "0" && hdnIsNewOrOld.Value != "")
                {
                    var extension = System.IO.Path.GetExtension(fileUpload.FileName);
                    if (extension.ToLower() == ".pdf")
                    {
                        if (fileUpload.PostedFile.ContentLength <= 10240 * 1024)
                        {
                            using (var conn = new SqlConnection(dbString))
                            {
                                conn.Open();

                                var cmd = new SqlCommand();
                                cmd.Connection = conn;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.CommandText = hdnIsNewOrOld.Value == "Old" ? "ProcAttatchFileToPatient" : "ProcInsertAndAttatchFileToPatient";

                                cmd.Parameters.Add("@doctorId", SqlDbType.VarChar).Value = ddlDoctor.SelectedValue;
                                cmd.Parameters.Add("@firstName", SqlDbType.VarChar).Value = txtFirstName.Text.Trim();
                                cmd.Parameters.Add("@lastName", SqlDbType.VarChar).Value = txtLastName.Text.Trim();
                                cmd.Parameters.Add("@filename", SqlDbType.VarChar).Value = System.IO.Path.GetFileName(fileUpload.FileName);

                                if (hdnIsNewOrOld.Value == "New")
                                {
                                    cmd.Parameters.Add("@salutation", SqlDbType.VarChar).Value = txtSalutation.Text.Trim();
                                    cmd.Parameters.Add("@middleName", SqlDbType.VarChar).Value = txtMiddleName.Text.Trim();
                                    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = txtEmail.Text.Trim();
                                    cmd.Parameters.Add("@sex", SqlDbType.VarChar).Value = ddlSex.SelectedValue;
                                    cmd.Parameters.Add("@ssn", SqlDbType.VarChar).Value = txtSsnNo.Text.Trim();
                                    cmd.Parameters.Add("@mrn", SqlDbType.VarChar).Value = txtMrnNo.Text.Trim();
                                    cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = txtPhone.Text.Trim();
                                    cmd.Parameters.Add("@dob", SqlDbType.VarChar).Value = txtDob.Text.Trim();
                                }


                                var serverPath = Server.MapPath("~/Reports/" + DateTime.Now.ToString("MMddyyyy") + "/");
                                var dir = new System.IO.DirectoryInfo(serverPath);
                                if (!dir.Exists)
                                    dir.Create();

                                fileUpload.SaveAs(serverPath + System.IO.Path.GetFileName(fileUpload.FileName));

                                try
                                {
                                    cmd.ExecuteScalar();
                                    flag = true;
                                }
                                catch (Exception ex)
                                {
                                    flag = false;
                                }
                            }
                        }
                        else
                            lblStatus.Text = "Please upload a file of size less than or equal to 4MB";
                    }
                    else
                        lblStatus.Text = "Please upload pdf files..";
                }
                else
                {
                    if (ddlDoctor.SelectedValue == "0")
                    {
                        lblStatus.Text = "Please select doctor..";
                    }
                    else if (!fileUpload.HasFile)
                    {
                        lblStatus.Text = "Please upload the file..";
                    }
                    else if (hdnIsNewOrOld.Value == "")
                    {
                        lblStatus.Text = "Please search a patient or add new patient";
                    }
                }
            }
            catch (Exception ex)
            {

                lblStatus.Text = ex.Message;
            }

            if (flag)
                Response.Redirect("admin.aspx");
        }
    }

    public class Search
    {
        public string text { get; set; }
        public string doctor { get; set; }
    }
    
}