﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;

namespace TransMed
{    
    public partial class groupdetails : System.Web.UI.Page
    {
        protected static string dbString = ConfigurationManager.ConnectionStrings["dbConn"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                grdGroupDetails.DataSource = GetGroupDetails();
                grdGroupDetails.DataBind();
            }
        }

        public DataTable GetGroupDetails()
        {
            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.CommandText = "ProcGetAllGroupDetails";

                var dt = new DataTable();

                dt.Load(cmd.ExecuteReader());

                return dt;
            }
        }

        [WebMethod]
        public static string GetGroupInfoById(int id)
        {
            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.CommandText = "ProcGetGroupInfoById";

                cmd.Parameters.Add("@id", SqlDbType.Int);
                cmd.Parameters["@id"].Value = id;

                var dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                var gInfo = new GroupInfo();
                if (dt.Rows.Count > 0)
                {
                    gInfo.GroupName = dt.Rows[0]["GroupName"].ToString();
                    gInfo.Phone = dt.Rows[0]["Phone"].ToString();
                    gInfo.ContactName = dt.Rows[0]["ContactName"].ToString();
                    gInfo.Address = dt.Rows[0]["Address"].ToString();
                    gInfo.Email = dt.Rows[0]["Email"].ToString();
                    gInfo.Fax = dt.Rows[0]["Fax"].ToString();

                }

                return Newtonsoft.Json.JsonConvert.SerializeObject(gInfo);
            }            
        }       

        protected void grdGroupDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdGroupDetails.PageIndex = e.NewPageIndex;

            grdGroupDetails.DataSource = GetGroupDetails();
            grdGroupDetails.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (var conn = new SqlConnection(dbString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ProcInsertUpdateGroupInfoById";

                cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = hdnGroupId.Value;
                cmd.Parameters.Add("@groupName", SqlDbType.VarChar).Value = txtGroupName.Text;
                cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = txtPhone.Text;
                cmd.Parameters.Add("@contactname", SqlDbType.VarChar).Value = txtContactName.Text;
                cmd.Parameters.Add("@address", SqlDbType.VarChar).Value = txtAddress.Text;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = txtEmail.Text;
                cmd.Parameters.Add("@fax", SqlDbType.VarChar).Value = txtFax.Text;

                cmd.ExecuteScalar();

                Response.Redirect("~/groupdetails.aspx");
            }
        }
    }

    public class GroupInfo
    {
        public string GroupName { get; set; }
        public string Phone { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
    }
}
