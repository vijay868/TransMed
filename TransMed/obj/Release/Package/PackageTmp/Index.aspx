﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="TransMed.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LayOuthead" runat="server">
    <link href="bower_components/bootstrap/dist/css/bootstrap-datetimepicker.css" rel="stylesheet" />
    <style type="text/css">
        /*
        .datepicker table tr td.active:hover, 
        .datepicker table tr td.active:hover:hover, 
        .datepicker table tr td.active.disabled:hover, 
        .datepicker table tr td.active.disabled:hover:hover, 
        .datepicker table tr td.active:active, 
        .datepicker table tr td.active:hover:active, 
        .datepicker table tr td.active.disabled:active, 
        .datepicker table tr td.active.disabled:hover:active, 
        .datepicker table tr td.active.active, 
        .datepicker table tr td.active:hover.active, 
        .datepicker table tr td.active.disabled.active, 
        .datepicker table tr td.active.disabled:hover.active, 
        .datepicker table tr td.active.disabled, 
        .datepicker table tr td.active:hover.disabled, 
        .datepicker table tr td.active.disabled.disabled, 
        .datepicker table tr td.active.disabled:hover.disabled, 
        .datepicker table tr td.active[disabled], 
        .datepicker table tr td.active:hover[disabled], 
        .datepicker table tr td.active.disabled[disabled], 
        .datepicker table tr td.active.disabled:hover[disabled]{
            background-image: none;
            background-color: white;
            font-weight: normal;
            color: black;
            text-shadow: none;
        }
        */
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LayOutBody" runat="server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h4>&nbsp </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 total_records">
                <span><b>
                    <asp:Label ID="lblRecords" runat="server"></asp:Label></b></span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 grdFiles-main">
                <asp:GridView ID="grdFiles" GridLines="None" runat="server" OnRowDataBound="grdFiles_RowDataBound" AlternatingRowStyle-CssClass="altRowCss" AutoGenerateColumns="false" AllowPaging="true" AllowSorting="false" OnPageIndexChanging="grdFiles_PageIndexChanging"
                    OnSorting="grdFiles_Sorting" PageSize="10" PagerSettings-PageButtonCount="3" CssClass="table table-condensed tblCss">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <input type="checkbox" id="chkHdr" class="checkbox" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <input type="checkbox" id="chk_<%# Eval("FileId") %>" class="chkCls checkbox" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DoctorName" HeaderText="Doctor Name" SortExpression="DoctorName" />
                        <asp:BoundField DataField="Name" HeaderText="Patient Name" SortExpression="Name" />
                        <asp:BoundField DataField="SSNNo" HeaderText="SSN#" SortExpression="SSNNo" />
                        <asp:BoundField DataField="MRN" HeaderText="MRN#" SortExpression="MRN" />
                        <asp:BoundField DataField="DateCreated" HeaderText="Date" SortExpression="DateCreated" />
                        <asp:TemplateField HeaderText="#Action">
                            <ItemTemplate>
                                <asp:Literal ID="ltrlBtnViewPdf" runat="server"></asp:Literal>
                                <asp:Literal ID="ltrlDownload" runat="server"></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
                <div class="download_btn">
                    <asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary btn-sm" OnClick="btnDownload_Click" OnClientClick="return btnDownload_Onclick();"><i class="fa fa-download"></i>&nbsp;Download</asp:LinkButton>
                </div>
            </div>
            <div class="col-lg-12">
                <asp:Label ID="lblGrdStatus" runat="server" Text="No records found." CssClass="lblGrdStatusCss"></asp:Label>
                <asp:HiddenField ID="hdnTypeOfSearch" runat="server" />
            </div>
        </div>
    </div>
    <div class="modal fade" id="pdfModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <iframe src="#" id="pdfIFrame" width="100%" height="550"></iframe>
            </div>
        </div>
    </div>
    <div class="modal fade" id="adSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Advance Search</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="txtFromDt">From Date</label>
                            <asp:TextBox ID="txtFromDt" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <label for="txtToDt">To Date</label>
                            <asp:TextBox ID="txtToDt" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="txtpLastName">Patient Last Name</label>
                            <asp:TextBox ID="txtpLastName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <label for="txtpFirstName">Patient First Name</label>
                            <asp:TextBox ID="txtpFirstName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="txtMrn">MRN No</label>
                            <asp:TextBox ID="txtMrn" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <label for="txtSsn">SSN No</label>
                            <asp:TextBox ID="txtSsn" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <asp:Panel runat="server" CssClass="row" ID="ppnlDoctor">
                        <div class="col-md-6">
                            <label for="doctor">Doctor</label>
                            <asp:DropDownList ID="doctor" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </asp:Panel>
                </div>
                <div class="modal-footer">
                    <label id="modalStatus" style="color: red; display: none;" class="pull-left">Please enter atleast one field.</label>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
                    <asp:LinkButton ID="lnkAdSearch" runat="server" CssClass="btn btn-primary btn-sm" OnClientClick="return lnkAdSearch();" OnClick="lnkAdSearch_Click"><i class="fa fa-search"></i>&nbsp;Search</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="navSearch" runat="server">
    <li class="sidebar-search">
        <div class="input-group custom-search-form">
            <asp:TextBox ID="txtSearch" runat="server" placeholder="Search..." CssClass="form-control"></asp:TextBox>
            <span class="input-group-btn">
                <asp:LinkButton ID="lnkSearch" runat="server" CssClass="btn btn-default" OnClientClick="return lnkSearch();" OnClick="lnkSearch_Click">
                    <i class="fa fa-search"></i>
                </asp:LinkButton>
            </span>
            <div style="display: none;">
                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-default" OnClientClick="return lnkSearch();" OnClick="lnkSearch_Click" />
            </div>
        </div>
        <div class="advanced-search">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#adSearch">
                Advanced Search &nbsp; <i class="fa fa-search"></i>
            </button>
            <div class="clearfix"></div>
        </div>
    </li>
    <li>
        <asp:Panel ID="pnlDoctor" runat="server" CssClass="well" Style="padding: 5px; background: none; border: 0px; margin: 10px 7px; box-shadow: none;">
            <asp:DropDownList ID="ddlDoctor" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDoctor_SelectedIndexChanged"></asp:DropDownList>
        </asp:Panel>
        <div class="well">
            <div class="searchby-calndr">
                <div class="form-group">
                    <div class="input-group date" id="dp6">
                        <input type='text' class="form-control" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <asp:HiddenField ID="hdnDate" runat="server" />
                <div style="display: none;">
                    <asp:LinkButton ID="lnkDate" runat="server" OnClick="lnkDate_Click"></asp:LinkButton>
                </div>
            </div>
        </div>

    </li>
    <asp:HiddenField ID="hdnUrl" runat="server" />
    <asp:HiddenField ID="hdnchkval" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LayOutBottom" runat="server">
    <script src="bower_components/jquery/dist/jquery.validate.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/moment.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript">
        function viewPdf(fileId, fileName, date) {
            $('#pdfIFrame').attr('src', 'web/viewer.html?file=' + fileName + '&date=' + moment(date).format('MMDDYYYY') + '&url=' + '192.168.0.224/TransMed' + '&ts=' + $.now())
            $('#pdfModal').modal('show');
            UpdateisViewed(fileId);
        }

        function UpdateisViewed(fileId) {

            var obj = { fileId: fileId };
            $.ajax({
                url: 'Index.aspx/Viewed',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(obj),
                success: function (res) {
                    $('#btnViewPdf_' + fileId).removeClass('btn-warning').addClass('btn-info');
                },
                error: function (err) {

                },
                failure: function () { }
            });
        }

        function lnkSearch() {
            var search = $('#ctl00_navSearch_txtSearch').val();
            if (search == null || search == '')
                return false;
            else
                return true;
        }

        $(document).keypress(function (e) {
            if (e.which == 13) {
                $('#ctl00_navSearch_btnSearch').click();
                return false;
            }
        });

        $(function () {
            $('#dp6').datetimepicker({
                <% if (hdnTypeOfSearch.Value != "0")
                   { %>
                    defaultDate: '<%= hdnDate.Value %>', 
                <% }
                   else
                   { %>
                    useCurrent: false,
                <% } %>
                    maxDate: moment('<%= DateTime.Now.ToString("MM/dd/yyyy") %>'),
                    format: 'MM/DD/YYYY',
                    inline: false
                });

                $('#ctl00_LayOutBody_txtFromDt, #ctl00_LayOutBody_txtToDt').datetimepicker({
                    format: 'MM/DD/YYYY',
                    inline: false,
                    maxDate: moment('<%= DateTime.Now.ToString("MM/dd/yyyy") %>')
            });

                $('#ctl00_LayOutBody_txtFromDt').on('dp.change', function (e) {
                    $('#ctl00_LayOutBody_txtToDt').data('DateTimePicker').minDate(e.date);
                });
                $('#ctl00_LayOutBody_txtToDt').on('dp.change', function (e) {
                    $('#ctl00_LayOutBody_txtFromDt').data('DateTimePicker').maxDate(e.date);
                });

                $('#dp6').on('dp.change', function (e) {
                    var dt = moment(e.date).format('MM/DD/YYYY');
                    $('#ctl00_navSearch_hdnDate').val(dt);
                    __doPostBack('ctl00$navSearch$lnkDate', '');
                });

                $("#chkHdr").click(function (event) {
                    if (this.checked) {
                        $('.chkCls').each(function () {
                            this.checked = true;
                        });
                    } else {
                        $('.chkCls').each(function () {
                            this.checked = false;
                        });
                    }
                });

            <% if (hdnTypeOfSearch.Value == "0")
               { %>
                $('.today').removeClass('active');
            <% } %>

            });

        function lnkAdSearch() {
            var firstName = $('#ctl00_LayOutBody_txtpFirstName').val();
            var lastName = $('#ctl00_LayOutBody_txtpLastName').val();
            var mrn = $('#ctl00_LayOutBody_txtMrn').val();
            var ssn = $('#ctl00_LayOutBody_txtSsn').val();
            var groupName = $('#ctl00_LayOutBody_txtGroupName').val();
            var frmDt = $('#ctl00_LayOutBody_txtFromDt').val();
            var toDt = $('#ctl00_LayOutBody_txtToDt').val();

            if (!isNullOrEmpty(firstName) || !isNullOrEmpty(lastName) || !isNullOrEmpty(frmDt) || !isNullOrEmpty(toDt) || !isNullOrEmpty(mrn) || !isNullOrEmpty(ssn)) {
                $('#modalStatus').fadeOut();
                return true;
            }
            else {
                $('#modalStatus').fadeIn();
                return false;
            }
        }

        function isNullOrEmpty(str) {
            if (str != null && str != '')
                return false;
            else
                return true;
        }

        function btnDownload_Onclick() {
            var isValid = false;
            var chkid = "";
            $('.chkCls').each(function () {
                if (this.checked) {
                    isValid = true;
                    chkid += this.id.split("_")[1] + ",";
                }
            });
            var n = chkid.lastIndexOf(",");
            chkid = chkid.substring(0, n);

            $("#ctl00_navSearch_hdnchkval").val(chkid);
            return isValid;
        }

        function download(date, filename, id) {
            $('#' + id).removeClass('btn-success').addClass('btn-primary');
        }

    </script>
</asp:Content>
