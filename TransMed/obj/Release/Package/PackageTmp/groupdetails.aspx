﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="groupdetails.aspx.cs" Inherits="TransMed.groupdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LayOuthead" runat="server">
    <style>
        .add-group {
            margin-bottom: 10px;
            margin-top: 20px;
        }
    </style>
    <script type="text/javascript">
        function Edit(id) {
            $.ajax({
                url: 'groupdetails.aspx/GetGroupInfoById?ts=' + $.now(),
                contentType: 'application/json',
                type: 'POST',
                data: JSON.stringify({ id: id }),
                success: function (res) {
                    var obj = JSON.parse(res.d);
                    $('#ctl00_LayOutBody_hdnGroupId').val(id);
                    $('#ctl00_LayOutBody_txtGroupName').val(obj.GroupName);
                    $('#ctl00_LayOutBody_txtPhone').val(obj.Phone);
                    $('#ctl00_LayOutBody_txtContactName').val(obj.ContactName);
                    $('#ctl00_LayOutBody_txtAddress').val(obj.Address);
                    $('#ctl00_LayOutBody_txtEmail').val(obj.Email);
                    $('#ctl00_LayOutBody_txtFax').val(obj.Fax);
                },
                error: function (err) {
                }
            });
        }

        function Add() {
            $('#myModalLabel').text('Add New Group');
            $('#ctl00_LayOutBody_hdnGroupId, #ctl00_LayOutBody_txtGroupName, #ctl00_LayOutBody_txtPhone, #ctl00_LayOutBody_txtContactName, #ctl00_LayOutBody_txtAddress, #ctl00_LayOutBody_txtEmail, #ctl00_LayOutBody_txtFax').val('');
            $('#ctl00_LayOutBody_hdnGroupId').val(-1);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LayOutBody" runat="server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-6">
                <h3>Group Details</h3>
            </div>
            <div class="col-md-6">
                <button type="button" data-toggle="modal" data-target="#EditModal" onclick="Add();" class="btn btn-primary btn-sm pull-right add-group">Add Group</button></div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <asp:GridView ID="grdGroupDetails" runat="server" CssClass="table table-hover table-bordered" AutoGenerateColumns="false" AllowPaging="true" OnPageIndexChanging="grdGroupDetails_PageIndexChanging" PageSize="10">
                    <Columns>
                        <asp:BoundField DataField="GroupName" HeaderText="Group Name" />
                        <asp:BoundField DataField="ContactName" HeaderText="Contact Name" />
                        <asp:BoundField DataField="Address" HeaderText="Address" />
                        <asp:BoundField DataField="Email" HeaderText="Email" />
                        <asp:BoundField DataField="Fax" HeaderText="Fax" />
                        <asp:TemplateField HeaderText="#Action">
                            <ItemTemplate>
                                <a href="#" onclick="Edit(<%# Eval("GroupId") %>)" data-toggle="modal" data-target="#EditModal">Edit</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Group Info</h4>
                </div>
                <div class="modal-body txtGroupName">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="txtGroupName">Group Name</label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox ID="txtGroupName" runat="server" CssClass="form-control"></asp:TextBox></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="txtPhone">Phone</label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control"></asp:TextBox></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="txtContactName">Contact Name</label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox ID="txtContactName" runat="server" CssClass="form-control"></asp:TextBox></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="txtAddress">Address</label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox ID="txtAddress" TextMode="MultiLine" Rows="5" runat="server" CssClass="form-control"></asp:TextBox></div>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="row">
                            <div class="col-md-3">
                                <label for="txtEmail">Email</label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="txtFax">Fax</label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox></div>
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnGroupId" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnSave_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
