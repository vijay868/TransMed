﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="TransMed.login" %>

<!DOCTYPE html>
<html lang="en">
<head>    
    <title>TransMed</title>    
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">    
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        label.error {
            color:red;
        }

        input[type="text"].error {
            border: 1px solid #f00;
            
            -moz-transition: all .5s;
            -webkit-transition: all .5s;
            transition: all .5s;
        }
        .lblStatus{
            color:red;
            font-weight:bold;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" runat="server" id="frmLogin">
                            <fieldset>
                                <div class="form-group">                                    
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" Placeholder="Email"></asp:TextBox>
                                </div>
                                <div class="form-group">                                    
                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" Placeholder="Password" TextMode="Password"></asp:TextBox>
                                </div>
                                <asp:Button ID="btnLoin" runat="server" CssClass="btn btn-lg btn-success btn-block" OnClientClick="Login()" OnClick="btnLoin_Click" Text="Submit" />
                            </fieldset>
                            <asp:Label ID="lblStatus" runat="server" Visible="false" CssClass="lblStatus"></asp:Label>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <script src="bower_components/jquery/dist/jquery.min.js"></script>    
    <script src="bower_components/jquery/dist/jquery.validate.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>    
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>    
    <script src="dist/js/sb-admin-2.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#frmLogin').validate({
                rules: {
                    txtEmail: {
                        required: true,
                        email: true
                    },
                    txtPassword: {
                        required: true
                    }
                }
            });
        });

        function Login() {
            return $('#frmLogin').valid();
        }
    </script>
</body>
</html>
