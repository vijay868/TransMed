﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace TransMed
{
    public class download : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string filePath = context.Server.MapPath("~/Reports/" + context.Request.QueryString["date"] + "/" + context.Request.QueryString["fileName"]);

            string fileId = context.Request.QueryString["fileId"];
            
            
            try
            {
                var fEx = new FileNotFoundException();
                FileInfo f = new FileInfo(filePath);
                
                if (f.Exists)
                {
                    PageBase.isViewedOrDownloaded(fileId, "ProcUpdateIsDownloaded");
                    context.Response.Clear();
                    context.Response.ContentType = "application/octet-stream";
                    context.Response.AddHeader("Content-Disposition", "attachment;filename=" + f.Name);
                    context.Response.TransmitFile(filePath);
                    context.Response.End();
                }
                else
                    throw fEx;
            }
            catch (FileNotFoundException ex)
            {
                context.Response.Redirect("error_404.html");
            }
            catch (Exception ex)
            {

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}