﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="TransMed.admin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Admin</title>
    <link href="bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
    <link href="bower_components/bootstrap/dist/css/bootstrap-datetimepicker.css" rel="stylesheet" />    
    <link href="bower_components/jquery/dist/jquery-ui.css" rel="stylesheet" />    
    <style type="text/css">
        body {
            min-height: 2000px;
            padding-top: 70px;
        }

        .hideRows {
            display: none;
        }

        label.error {
            color: red;
        }

        input[type="text"].error {
            border: 1px solid #f00;
            -moz-transition: all .5s;
            -webkit-transition: all .5s;
            transition: all .5s;
        }

        .lblStatusCss {
            font-weight:bold;
            color:red;
        }
    </style>
    <script type="text/javascript">
        var ComArray = new Array();

        function btnSubmitClick() {
            return $('#frm').valid();
        }
    </script>
</head>
<body>
    <form id="frm" runat="server">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">TransMed Admin</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <%--<ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#contact">Contact</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li class="dropdown-header">Nav header</li>
                                <li><a href="#">Separated link</a></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Default</a></li>
                        <li><a href="#">Static top</a></li>
                        <li class="active"><a href="#">Fixed top <span class="sr-only">(current)</span></a></li>
                    </ul>--%>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <label for="ddlDoctor">Select Doctor</label>
                    <asp:DropDownList ID="ddlDoctor" runat="server" CssClass="form-control" onchange="ddlDoctorChanged()">
                    </asp:DropDownList>
                </div>
                <div class="col-md-8">&nbsp;</div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label for="txtPatientName">Search/(fname/lname/mrn/ssn)</label>
                    <asp:TextBox ID="txtPatientName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-4" style="padding-top: 25px;">
                    <button id="add" type="button" onclick="btnAdd();" class="btn btn-primary btn-block">Add New</button>
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row hideRows">
                <div class="col-md-4">
                    <label for="txtFirstName">Patient First Name</label>
                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-4">
                    <label for="txtLastName">Patient Last Name</label>
                    <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="row hideRows">
                <div class="col-md-4">
                    <label for="txtMiddleName">Patient Middle Name</label>
                    <asp:TextBox ID="txtMiddleName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-4">
                    <label for="txtMrn">MRN No.</label>
                    <asp:TextBox ID="txtMrnNo" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="row hideRows">
                <div class="col-md-4">
                    <label for="txtPhone">Phone</label>
                    <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-4">
                    <label for="txtSsnNo">SSN No.</label>
                    <asp:TextBox ID="txtSsnNo" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="row hideRows">
                <div class="col-md-4">
                    <label for="ddlSex">Sex</label>
                    <asp:DropDownList ID="ddlSex" runat="server" CssClass="form-control">
                        <asp:ListItem Text="Male" Value="male">Male</asp:ListItem>
                        <asp:ListItem Text="Female" Value="female">Female</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-4">
                    <label for="txtEmail">Email</label>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="row hideRows">
                <div class="col-lg-4">
                    <label for="txtSalutation">Salutation</label>
                    <asp:TextBox ID="txtSalutation" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-4">
                    <label for="txtDob">DOB</label>
                    <asp:TextBox ID="txtDob" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <label for="fileUpload">File Upload</label>
                    <asp:FileUpload ID="fileUpload" runat="server" CssClass="form-control" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-8" style="padding-top: 25px;">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save" CssClass="btn btn-primary btn-block" OnClientClick="btnSubmitClick();" OnClick="btnSubmit_Click" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-12"><asp:Label ID="lblStatus" runat="server" CssClass="lblStatusCss"></asp:Label></div>
            </div>
        </div>
        <asp:Literal ID="ltrlArray" runat="server"></asp:Literal>  
        <asp:HiddenField ID="hdnIsNewOrOld" runat="server" />
    </form>    
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/jquery/dist/jquery-ui.js"></script>
    <script src="bower_components/jquery/dist/jquery.validate.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script src="bower_components/bootstrap/dist/js/moment.min.js"></script> 
    <script src="bower_components/bootstrap/dist/js/bootstrap-datetimepicker.min.js"></script>    
    <script type="text/javascript">
        $(function () {
            $('#txtDob').datetimepicker({
                format: 'MM/DD/YYYY',
                inline: false,
                maxDate: moment('<%= DateTime.Now.ToString("MM/dd/yyyy") %>')
            });

            $('#txtPatientName').autocomplete({
                source: function (request, response) {
                    var param = { text: $('#txtPatientName').val(), doctor: $('#ddlDoctor').val() };
                    var _Obj = { Obj: param };
                    $.ajax({
                        url: 'admin.aspx/GetPatientsByDoctorId',
                        data: JSON.stringify(_Obj),
                        dataType: 'json',
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            response($.map(JSON.parse(data.d), function (item) {
                                return {
                                    value: item.name
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                },
                select: function (event, ui) {
                    if (ui.item) {
                        $('#hdnIsNewOrOld').val('Old');
                        GetPatientInfo(ui.item.value, $('#ddlDoctor').val());
                    }
                },
                minLength: 2
            });

            $('#frm').validate({
                rules: {
                    txtFirstName: {
                        required: true
                    },
                    txtLastName: {
                        required: true
                    },
                    txtMrnNo: {
                        required: true
                    },
                    txtPhone: {
                        required: true
                    },
                    txtSsnNo: {
                        required: true
                    },
                    ddlSex: {
                        required: true
                    },
                    txtEmail: {
                        required: true,
                        email: true
                    },
                    txtDob: {
                        required: true
                    },
                    ddlDoctor: {
                        required: true
                    }
                }
            });
        });

        function GetPatientInfo(text, doctor) {
            var obj = {
                text: text,
                doctor: doctor
            };

            var data = { Obj: obj };
            $.ajax({
                url: 'admin.aspx/PatientInfo',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(data),
                success: function (res) {

                    var jObj = JSON.parse(res.d)[0];
                    $('#txtFirstName').val(jObj.FirstName).attr('readonly', 'readonly');
                    $('#txtLastName').val(jObj.LastName).attr('readonly', 'readonly');
                    $('#txtMiddleName').val(jObj.MiddleName).attr('readonly', 'readonly');
                    $('#txtMrnNo').val(jObj.MRN).attr('readonly', 'readonly');
                    $('#txtPhone').val(jObj.Phone).attr('readonly', 'readonly');
                    $('#txtSsnNo').val(jObj.SsnNo).attr('readonly', 'readonly');
                    $('#txtEmail').val(jObj.Email).attr('readonly', 'readonly');
                    $('#ddlSex').val(jObj.Sex).attr('readonly', 'readonly');
                    $('#txtSalutation').val(jObj.Salutation).attr('readonly', 'readonly');
                    $('#txtDob').val(jObj.DOB).attr('readonly', 'readonly');
                    $('.hideRows').fadeIn();

                    //$('#btnSubmit').hide();
                }
            });
        }

        function btnAdd() {
            $('.hideRows').fadeIn();

            $('#txtFirstName').val('').removeAttr('readonly');
            $('#txtLastName').val('').removeAttr('readonly');
            $('#txtMiddleName').val('').removeAttr('readonly');
            $('#txtMrnNo').val('').removeAttr('readonly');
            $('#txtPhone').val('').removeAttr('readonly');
            $('#txtSsnNo').val('').removeAttr('readonly');
            $('#ddlSex').val('').removeAttr('readonly');
            $('#txtEmail').val('').removeAttr('readonly');
            $('#txtSalutation').val('').removeAttr('readonly');
            $('#txtDob').val('').removeAttr('readonly');
            $('#btnSubmit').show();

            $('#hdnIsNewOrOld').val('New');
        }

        function ddlDoctorChanged() {
            $('#txtPatientName').val('');

            $('.hideRows').fadeOut();
        }
    </script>
</body>
</html>
